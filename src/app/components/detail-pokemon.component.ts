import { Component, OnInit, NgZone } from '@angular/core';
import { PokemonService } from '../shared/pokemon.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html'
})

export class DetailPokemonComponent implements OnInit {
  pokemonDetail: any = [];
  
  constructor(
    private titleService:Title,
    private actRoute: ActivatedRoute,    
    public pokemonService: PokemonService,
    private ngZone: NgZone,
    private router: Router
  ) { 
    let url: any = this.actRoute.snapshot.paramMap.get('id');
    this.pokemonService.GetPokemonDetail(url)
          .then(response => {
            this.pokemonDetail = response?.data;
            this.titleService.setTitle(`Pokemon App - ${response.data.name}`);
          })
          .catch(error => {
            console.log(error);
          });
  }

  ngOnInit() {
    
  }

  back(){
    this.ngZone.run(() => this.router.navigateByUrl('/home'));
  }
}