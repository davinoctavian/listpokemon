import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../shared/pokemon.service';
import { Title } from "@angular/platform-browser";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    PokemonList: any = [];
    countData: number = 0;
    page: number = 1;

    constructor(
        private titleService:Title,
        public pokemonService: PokemonService
    ){ 
        this.titleService.setTitle("Pokemon App - Davin Octavian");
    }

    ngOnInit() {
        this.loadPokemon();
    }

    // pokemon list
    loadPokemon() {
        this.pokemonService.GetPokemonList(this.page, 25)
          .then(response => {
            this.PokemonList = response?.data?.results;
            this.countData = response?.data?.count
          })
          .catch(error => {
            console.log(error);
          });
    }

    onTableChange(event: any) {
        this.page = event
        this.pokemonService.GetPokemonList(event, 25)
          .then(response => {
            this.PokemonList = response?.data?.results;
            this.countData = response?.data?.count
          })
          .catch(error => {
            console.log(error);
          });
    }
}