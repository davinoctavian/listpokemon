import { Injectable } from '@angular/core';
import axios from 'axios';

@Injectable({
  providedIn: 'root',
})

export class PokemonService {
    // Base url
    baseurl = 'https://pokeapi.co/api/v2';
    constructor() {}

    // GET list pokemon
    GetPokemonList(off: number, limit: number) {
      return axios.get(`${this.baseurl}/pokemon`, {
        headers: {
          'Content-Type': 'application/json',
        },
        params : {
          offset: (off - 1) * 25,
          limit: limit
        }
      });
    }

    //GET pokemon detail
    GetPokemonDetail(url:string) {
      return axios.get(url, {
        headers: {
          'Content-Type': 'application/json',
        }
      });
    }
  
}